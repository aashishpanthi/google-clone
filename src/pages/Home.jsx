import React from 'react'
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import AppsIcon from '@material-ui/icons/Apps'
import '../ess/css/Home.css'
import { Link } from 'react-router-dom'
import Search from '../components/Search';

function Home() {
    return (
        <div className='home'>
            <div className="home__header">
                <div className="home__headerLeft">
                    <Link to='/about'>
                        About
                    </Link>

                    <Link to='/store'>
                        Store
                    </Link>
                </div>

                <div className="home__headerRight">
                    <Link to='/gamil'>
                        Gmail
                    </Link>

                    <Link to='/images'>
                        Images
                    </Link>
                    <AppsIcon />
                    <AccountCircleIcon />
                </div>
            </div>

            <div className="home__body">
                <img src='https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png' alt='google logo' />

                <Search />
            </div>
        </div>
    )
}

export default Home
