import React,{useState} from 'react'
import SearchIcon from '@material-ui/icons/Search'
import MicIcon from '@material-ui/icons/Mic'
import '../ess/css/Search.css'
import { Button } from '@material-ui/core'
import { useHistory } from 'react-router-dom'
import { useStateValue } from '../StateProvider'
import { actionTypes } from '../reducer'

function Search({hideButtons = false}) {
    const [state, dispatch] = useStateValue()
    const [searchInput, setSearchInput] = useState('')
    const history = useHistory()

    const search = e =>{
        e.preventDefault()

        dispatch({
            type: actionTypes.SET_SEARCH_TERM,
            term: searchInput,
        })
        history.push(`/search/${searchInput}`)
    }

    return (
        <form className='search'>
            <div className="search__input">
                <SearchIcon className='search__inputIcon' />
                <input value={searchInput} onChange={e =>setSearchInput(e.target.value) } />
                <MicIcon />
            </div>

            <div className={`search__buttons ${hideButtons && 'search__buttons-hideButtons'}`}>
                <Button type='submit' onClick={search}> Google Search </Button>
                <Button variant='outlined'> I'm Feeling Lucky </Button>
            </div>
        </form>
    )
}

export default Search
